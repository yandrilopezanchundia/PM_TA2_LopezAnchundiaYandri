package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6


        // reciben los parametros del object_id de la otra clase
        String valor = getIntent().getStringExtra("object_id");
        DataQuery dataQuery = DataQuery.get("item");
        dataQuery.getInBackground(valor, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {

                if (e == null){
                    //Buscamos los recursos que obtienen las id del TextView
                    TextView nombre = (TextView)findViewById(R.id.elNombre);
                    //Buscamos los recursos que obtienen las id del TextView
                    TextView precio = (TextView)findViewById(R.id.elPrecio);
                    //Buscamos los recursos que obtienen las id del ImageView
                    ImageView thumbnail = (ImageView)findViewById(R.id.thumbnail);
                    //Buscamos los recursos que obtienen las id del TextView
                    TextView descripcion = (TextView)findViewById(R.id.laDescripcion);

                    //Añade el nombre
                    nombre.setText((String) object.get("name"));
                    //Añade el precio
                    precio.setText((String) object.get("price")+"\u0024");
                    //Añade la imagen
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    //Añade la descripcion
                    descripcion.setText((String) object.get("description"));
                }else{
                    /////ERROR
                }
            }
        });



        // FIN - CODE6

    }



}
